package com.example.mobileservice;

/**
 *
 * @author vrg
 */
public class MobilePart {
    public MobilePartType type;
    public boolean failing;
    public String name;

    public MobilePart(MobilePartType type, boolean failing, String name) {
        this.type = type;
        this.failing = failing;
        this.name = name;
    }
    
    
}
