package com.example.mobileservice;

import com.example.mobileservice.supplier.MobilePartSupplier;
import com.example.mobileservice.supplier.TaskScheduler;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vrg
 */
public class Main {
    
    private MobilePhoneRepairService mobilePhoneRepairService;
    private MobilePartSupplier mobilePartSupplier;
    private final List<Client> clients = new ArrayList<>();
    private TaskScheduler scheduler = TaskScheduler.INSTANCE;
    
    public static void main(String[] args) {
        Main main = new Main();
        main.sendMobilesToService();
        main.startWorkAtMobileService();
    }

    public Main() {
        mobilePartSupplier = new MobilePartSupplier();
        mobilePhoneRepairService = new MobilePhoneRepairService(mobilePartSupplier);
        for (int i = 0; i < 100; i++) {
            Client client = new Client(mobilePhoneRepairService);
            clients.add(client);
        }
    }
    
    public void sendMobilesToService() {
        for (Client client : clients) {
            SendMobileToServiceAndPollStatusTask task = new SendMobileToServiceAndPollStatusTask(client);
            new Thread(task).start();
        }
    }
    
    public void startWorkAtMobileService() {
        while(true) {
            mobilePhoneRepairService.pollSupplier();
            mobilePhoneRepairService.processWorksheets();
            mobilePhoneRepairService.orderParts();
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
